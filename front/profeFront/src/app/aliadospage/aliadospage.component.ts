import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare const main: any;
@Component({
  selector: 'app-aliadospage',
  templateUrl: './aliadospage.component.html',
  styleUrls: ['./aliadospage.component.scss']
})
export class AliadospageComponent implements OnInit {
  aliados = [
    {
      nombre:'3DLink',
      imagen: 'assets/images/3dllink.png',
      extracto: 'Es un privilegio y un orgullo aportar nuestro conocimiento para el desarrollo de una plataforma que busque hacer aportes significativos en el área deportiva de nuestro país...',
      texto: 'Es un privilegio y un orgullo aportar nuestro conocimiento para el desarrollo de una plataforma que busque hacer aportes significativos en el área deportiva de nuestro país. La unión de fuerzas para concretar este proyecto ha sido posible gracias al compromiso social de 3DLINK, el cual nos obliga a trabajar en proyectos sin fines de lucro como este. El vínculo entre lo deportivo y lo tecnológico constituirá un aporte pequeño pero importante para el deporte que, sin duda, podrá observarse en los resultados positivos de los tiempos por venir.',
      url: '/aliado/3dlink'
    },
    {
      nombre:'Hipereventos',
      imagen: 'assets/images/hipereventos.png',
      extracto: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In mollis nunc sed id semper risus in id...',
      texto: 'Soy Hipereventos the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliados'
    },
    {
      nombre:'Bufete de Abogados',
      imagen: 'assets/images/law.jpg',
      extracto: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In mollis nunc sed id semper risus in in...',
      texto: 'Soy abogados the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliados'
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    main();
  }

}
