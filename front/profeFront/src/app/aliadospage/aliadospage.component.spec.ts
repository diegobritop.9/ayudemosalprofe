import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AliadospageComponent } from './aliadospage.component';

describe('AliadospageComponent', () => {
  let component: AliadospageComponent;
  let fixture: ComponentFixture<AliadospageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AliadospageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AliadospageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
