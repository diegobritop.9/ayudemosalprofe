import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare const main: any;
@Component({
  selector: 'app-testimonio',
  templateUrl: './testimonio.component.html',
  styleUrls: ['./testimonio.component.scss']
})
export class TestimonioComponent implements OnInit {
  
  myId= null;
  test:any;
  testimonios = [
    {
      nombre: "Tiffany Cornejo, Comunicadora Social",
      texto: "Siempre he pensado que no hay mayor satisfacción que la que te deja el poder contribuir con el bienestar de otro. \nCuando ayudas, siento que quien termina ganando eres tú y creo que todo el que haya experimentado esa satisfacción puede dar fe de ello.\n\nEs por esto, que en estos momentos de pandemia mundial que estamos viviendo, decido demostrar que en las crisis puede salir lo mejor del ser humano y apenas conocí de esta iniciativa decidí unirme.\n\n¿Quienes seríamos nosotros si no hubiésemos tenido un Profe que haya forjado nuestras bases, principios y valores?\n\nHoy, demostrándoles que sí nos ayudaron a ser mejores seres humanos, vamos a devolverle ese granito de arena, todos desde nuestra trinchera, podemos ayudar al profe!",
      foto: "assets/images/tiffany.png"
    },
    {
      nombre: "Carlos Domingues, Periodista Deportivo",
      texto: "Siempre he pensado que no hay mayor satisfacción que la que te deja el poder contribuir con el bienestar de otro. \nCuando ayudas, siento que quien termina ganando eres tú y creo que todo el que haya experimentado esa satisfacción puede dar fe de ello.\n\nEs por esto, que en estos momentos de pandemia mundial que estamos viviendo, decido demostrar que en las crisis puede salir lo mejor del ser humano y apenas conocí de esta iniciativa decidí unirme.\n\n¿Quienes seríamos nosotros si no hubiésemos tenido un Profe que haya forjado nuestras bases, principios y valores?\n\nHoy, demostrándoles que sí nos ayudaron a ser mejores seres humanos, vamos a devolverle ese granito de arena, todos desde nuestra trinchera, podemos ayudar al profe!",
      foto: "assets/images/carlos.png"
    }
  ]
  
  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    main();
    this.myId = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.myId);
    if(this.myId == '1'){
      this.test = this.testimonios[0];
    }else{
      this.test = this.testimonios[1];
    }
  }

}
