import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaptainsInfoComponent } from './captains-info.component';

describe('CaptainsInfoComponent', () => {
  let component: CaptainsInfoComponent;
  let fixture: ComponentFixture<CaptainsInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaptainsInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaptainsInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
