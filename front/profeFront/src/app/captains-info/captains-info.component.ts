import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-captains-info',
  templateUrl: './captains-info.component.html',
  styleUrls: ['./captains-info.component.scss']
})
export class CaptainsInfoComponent implements OnInit {

  capitanes = [
    {
      nombre:'Miku Fedor',
      imagen: 'assets/images/1.png',
      texto1: 'Jugador Profesional.',
      texto2: 'Vinotinto con más de 50 partidos y 10 goles oficiales con la Selección Nacional de Venezuela.',
      texto3: 'Equipo actual: AC Omonia Nicosia (Chipre).'
    },
    {
      nombre:'José Manuel Rey',
      imagen: 'assets/images/9.png',
      texto1: 'Ex-Jugador Profesional.',
      texto2:'Vinotinto con 110 partidos y 10 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Actualmente DT de Zamora F.C. (Venezuela)'
    },
    {
      nombre:'Grenddy Perozo',
      imagen: 'assets/images/2.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 43 partidos y 2 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: Atlético Venezuela (Venezuela)'
    },
    {
      nombre:'Roberto Rosales',
      imagen: 'assets/images/7.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 79 partidos y 1 gol oficial con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: CD Leganés (España)'
    },
    {
      nombre:'Luis Manuel Seijas',
      imagen: 'assets/images/8.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 70 partidos y 2 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: Independiente de Santa Fe (Colombia)'
    },
    {
      nombre:'Ronald Vargas',
      imagen: 'assets/images/6.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 21 partidos y 3 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: KV Oostende (Bélgica)'
    },
    {
      nombre:'Oswaldo Viscarrondo',
      imagen: 'assets/images/4.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 80 partidos y 7 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: ES Troyes AC (Francia).'
    },
    {
      nombre:'Miguel Mea Vitali',
      imagen: 'assets/images/11.png',
      texto1: 'Ex-Jugador Profesional',
      texto2:'Vinotinto con 84 partidos y 1 gol oficial con la Selección Nacional de Venezuela.',
      texto3:'Gerente Deportivo del Caracas F.C.'
    },
    {
      nombre:'Vicente Suanno',
      imagen: 'assets/images/10.png',
      texto1: 'Ex-Jugador Profesional',
      texto2:'Ex-Vinotinto Sub-20 y Preolímpico Sub-23.',
      texto3:'Director General del Dvo. La Guaira (Venezuela)'
    },
    {
      nombre:'Ricardo Andreutti',
      imagen: 'assets/images/3.png',
      texto1: 'Jugador Profesional.',
      texto2:'Equipo actual: Caracas F.C. (Venezuela)',
      texto3:''
    },
    {
      nombre:'Alejandro Guerra',
      imagen: 'assets/images/5.png',
      texto1: 'Jugador Profesional.',
      texto2:'Vinotinto con 59 partidos y 3 goles oficiales con la Selección Nacional de Venezuela.',
      texto3:'Equipo actual: Palmeiras (Brasil)'
    }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
