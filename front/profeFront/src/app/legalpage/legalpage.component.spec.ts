import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalpageComponent } from './legalpage.component';

describe('LegalpageComponent', () => {
  let component: LegalpageComponent;
  let fixture: ComponentFixture<LegalpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
