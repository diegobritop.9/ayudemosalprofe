import { Component, OnInit } from '@angular/core';

declare const main: any;
@Component({
  selector: 'app-legalpage',
  templateUrl: './legalpage.component.html',
  styleUrls: ['./legalpage.component.scss']
})
export class LegalpageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    main();
  }

}
