import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {

  gallery = [
    [
      {
        imagen: 'assets/images/galeria-1.png'
      },
      {
        imagen: 'assets/images/galeria-2.png'
      },
      {
        imagen: 'assets/images/galeria-3.png'
      },
      {
        imagen: 'assets/images/galeria-4.png'
      }
    ],
    [
      {
        imagen: 'assets/images/galeria-5.png'
      },
      {
        imagen: 'assets/images/galeria-6.png'
      },
      {
        imagen: 'assets/images/galeria-7.png'
      },
      {
        imagen: 'assets/images/galeria-8.png'
      }
    ]
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
