import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  items;
  contactForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder) {
    
  }

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      name: '',
      email: ['', [Validators.required, Validators.email]],
      mensaje: ['', Validators.required]
    });
  }

  onSubmit(customerData) {
    this.submitted = true;
    if (this.contactForm.invalid) {
      return;
    }
    this.onReset();
    console.log('Your order has been submitted', customerData);
  }

  onReset(){
    this.contactForm.reset();
    this.submitted = false;
  }

}
