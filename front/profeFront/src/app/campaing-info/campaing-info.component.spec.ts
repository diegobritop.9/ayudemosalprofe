import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaingInfoComponent } from './campaing-info.component';

describe('CampaingInfoComponent', () => {
  let component: CampaingInfoComponent;
  let fixture: ComponentFixture<CampaingInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaingInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaingInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
