import { Component, OnInit } from '@angular/core';

declare const main: any;
@Component({
  selector: 'app-donationpage',
  templateUrl: './donationpage.component.html',
  styleUrls: ['./donationpage.component.scss']
})
export class DonationpageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    main();
  }

}
