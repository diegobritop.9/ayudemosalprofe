import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';

declare const main: any;
declare var $: any;

interface Gender {
  value: string;
  viewValue: string;
};

interface Activeprofe {
  value: string;
  viewValue: string;
};

@Component({
  selector: 'app-registropage',
  templateUrl: './registropage.component.html',
  styleUrls: ['./registropage.component.scss']
})
export class RegistropageComponent implements OnInit {
  
  registroForm: FormGroup;
  // registroFormControl = new FormControl('', [
  //   Validators.required,
  //   Validators.email,
  // ]);

  // selectedValueGender: 'masculino';

  // gender: Gender[] = [
  //   {value: 'femenino', viewValue: 'Femenino'},
  //   {value: 'masculino', viewValue: 'Masculino'}
  // ];

  // selectedValueActive: 'No';

  // activeprofe: Activeprofe[] = [
  //   {value: 'Si', viewValue: 'Si'},
  //   {value: 'No', viewValue: 'No'}
  // ];
  

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {

    this.registroForm = this.formBuilder.group({
      nameprofe: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      gender: ['Masculino', [Validators.required]],
      dni: ['', [Validators.required]],
      card: [''],
      email: ['', [Validators.required, Validators.email]],
      num_home: [''],
      num_movil: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      activeprofe: [false],
      school: ['', [Validators.required]],
      contact: ['', [Validators.required]],
      num_contact: ['', [Validators.required]],
      asociacion: ['', [Validators.required]],
      category: ['', [Validators.required]],
      trayectoria: ['']
    });
  }

  ngAfterViewInit(): void {
    main();
  }

  activarProfe(activeprofe){
    console.log(activeprofe);
    console.log(this.registroForm.value)
  }


  onSubmit() {
    console.log(this.registroForm.value)
    if(this.registroForm.value.activeprofe){
      if(this.errorHandling('school', 'required') || 
      this.errorHandling('contact', 'required') || 
      this.errorHandling('num_contact', 'required') || 
      this.errorHandling('asociacion', 'required') || 
      this.errorHandling('category', 'required')){
        console.log('no se puede enviar');
      }else{
        console.log('delenomas');
      }
    }else{
      if(this.errorHandling('nameprofe', 'required') || 
      this.errorHandling('lastname', 'required') || 
      this.errorHandling('gender', 'required') || 
      this.errorHandling('dni', 'required') || 
      this.errorHandling('email', 'required') || 
      this.errorHandling('num_movil', 'required') || 
      this.errorHandling('city', 'required') || 
      this.errorHandling('state', 'required')){
        console.log('no se puede enviar nop');
      }else{
        console.log('delenomas sip');
      }
    }
  }

   /* Handle form errors in Angular 8 */
   public errorHandling = (control: string, error: string) => {
    return this.registroForm.controls[control].hasError(error);
  }

}
