import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pago3dlink',
  templateUrl: './pago3dlink.component.html',
  styleUrls: ['./pago3dlink.component.scss'],
})
export class Pago3dlinkComponent implements OnInit {
  currency: string;
  paymentMethod: string;
  paymentMethodId: any;
  paymentMethodsList: any;
  request: string;
  currencies = [
    { id: 1, name: 'USD' },
    { id: 2, name: 'Bs.S.' },
  ];

  constructor() {}

  async ngOnInit() {
    // this.request = this.route.snapshot.paramMap.get('request');
    // const valid = this.uuid.test(this.request);
    // if (!valid) {
    //   // Swal.fire({
    //   //   titleText: 'Solicitud invalida',
    //   //   text: 'La solicitud que esta tratando de tramitar es invalida',
    //   //   type: 'error',
    //   //   heightAuto: false
    //   // });
    //   this.router.navigate(['/']);
    // }
    // this.paymentMethodsList = [];
    // this.paymentMethodsList = await this.paymentMethods.getAll().toPromise();
  }

  changeCurrency({ value }) {
    // Para limpiar paymentMethod cuando cambie el tipo de divisa
    this.paymentMethod = '';
    this.currency = value;
    console.log('diego', value);
  }

  async changePaymentType({ value }) {
    this.paymentMethod = value;
    console.log(value);

    // this.getPaymentMethodId();
  }

  txSave() {
    alert('prueba');
  }

  ngAfterViewInit() {
    // Swal.close();
  }

  // async getPaymentMethodId() {
  //   this.paymentMethodId = this.paymentMethodsList.filter(ele => {
  //     if (ele.slug === this.paymentMethod) {
  //       return true;
  //     }
  //   })[0];
  // }

  // getPaymentMethodBySlug(slug) {
  //   const data = this.paymentMethodsList.filter(ele => {
  //     return ele.slug === slug;
  //   });
  //   return data;
  // }

  // isActiveMethod(slug: string) {
  //   const [{ is_active }] = this.getPaymentMethodBySlug(slug);
  //   return is_active || false;
  // }
  // openAccounts(e, type) {
  //   // e.preventDefault();
  //   // const dialog = this.dialog.open(EditDialogComponent, {
  //   //   width: '450px',
  //   //   data: { type, id: this.paymentMethodId.id }
  //   // });
  // }
}
