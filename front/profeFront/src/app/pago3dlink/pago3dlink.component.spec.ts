import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Pago3dlinkComponent } from './pago3dlink.component';

describe('Pago3dlinkComponent', () => {
  let component: Pago3dlinkComponent;
  let fixture: ComponentFixture<Pago3dlinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Pago3dlinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Pago3dlinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
