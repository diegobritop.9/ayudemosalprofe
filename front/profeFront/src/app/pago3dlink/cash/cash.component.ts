import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuild } from '../../_shared/form-build';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-cash',
  templateUrl: './cash.component.html',
  styleUrls: ['./cash.component.scss']
})
export class CashComponent implements OnInit, FormBuild {
  form: FormGroup;

  constructor(
    private router: Router
  ) {
    this.formBuild();
  }

  ngOnInit() {}

  formBuild() {
  }

}
