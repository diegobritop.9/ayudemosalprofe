import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuild } from '../../_shared/form-build';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-pago-movil',
  templateUrl: './pago-movil.component.html',
  styleUrls: ['./pago-movil.component.scss']
})
export class PagoMovilComponent implements OnInit, FormBuild {
  @Input() request: string;
  @Output() saved: EventEmitter<boolean> = new EventEmitter<boolean>();

  vpBanks: Array<any>;
  form: FormGroup;
  datePatter: RegExp;
  maxDate = new Date();
  constructor() {
    this.formBuild();
    // this.getVPBanks();
  }

  ngOnInit() {}

  async getVPBanks() {
    // const data = await this.vpBank.getPagoMovilAccounts();
    // this.vpBanks = data;
  }

  formBuild() {
    this.form = new FormGroup({
      reference: new FormControl('', [
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(8)
      ]),
      date: new FormControl('', [Validators.required]),
      bankRec: new FormControl('', [Validators.required]),
      observation: new FormControl('', Validators.maxLength(255))
    });
  }

  async save() {
    Swal.fire({
      title: 'Guardando',
      text: 'Estamos registrando su pago',
      heightAuto: false,
      onBeforeOpen() {
        Swal.showLoading();
      }
    });
    try {
      const dateOfPayment = moment(this.form.value.date).format('YYYY-MM-DD');
      // const payment = await this.payment
      //   .add({
      //     amount: 100,
      //     comments: this.form.value.observation,
      //     request: this.request,
      //     payment_date: dateOfPayment,
      //     reference: this.form.value.reference,
      //     vp_bank: this.form.value.bankRec
      //   })
      //   .toPromise();
      alert('guardó');
      this.saved.emit(true);
    } catch (e) {
      alert('error')
    }
  }
}
