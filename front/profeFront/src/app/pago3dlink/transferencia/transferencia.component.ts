import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transferencia',
  templateUrl: './transferencia.component.html',
  styleUrls: ['./transferencia.component.scss'],
})
export class TransferenciaComponent implements OnInit {
  @Output() saved: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() request: string;

  datePatter: RegExp;
  form: FormGroup;
  vpBanks: Array<any>;
  entities: Array<any>;
  maxDate = new Date();
  constructor() {
    this.formBuild();
    this.datePatter = /(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-[0-9]{4}/;
  }

  ngOnInit() {
    this.getVPBanks();
    this.getEntities();
  }

  async getVPBanks() {
    //   const data = await this.vpBank.getTransferenciasAccounts();
    //   this.vpBanks = data;
    this.vpBanks = [
      { id: 1, name: 'Banco 1' },
      { id: 2, name: 'Banco 2' },
    ];
  }

  async getEntities() {
    // const data = await this.Entities.getNacionalBanks();
    this.entities = [
      { id: 1, name: 'Banco 1' },
      { id: 2, name: 'Banco 2' },
    ];
  }
  formBuild() {
    this.form = new FormGroup({
      reference: new FormControl('', [
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(8),
      ]),
      origin_bank: new FormControl('', [Validators.required]),
      date: new FormControl('', [Validators.required]),
      vp_bank: new FormControl('', [Validators.required]),
      comments: new FormControl('', [Validators.maxLength(255)]),
    });
  }

  async save() {
    Swal.fire({
      title: 'Guardando',
      text: 'Estamos registrando su pago',
      heightAuto: false,
      onBeforeOpen() {
        Swal.showLoading();
      },
    });
    try {
      // const { pos_amount } = await this.Request.get(this.request).toPromise();
      // const dateOfPayment = moment(this.form.value.date).format('YYYY-MM-DD');
      // const payment = await this.payment
      //   .add({
      //     amount: pos_amount * 70.0,
      //     comments: this.form.value.comments,
      //     request: this.request,
      //     payment_date: dateOfPayment,
      //     reference: this.form.value.reference,
      //     origin_bank: this.form.value.origin_bank,
      //     vp_bank: this.form.value.vp_bank,
      //   })
      //   .toPromise();
      this.saved.emit(true);
    } catch (e) {
      // Swal.fire({
      //   titleText: 'Error',
      //   text:
      //     'Ha occurido un error en el proceso, por favor verifique su conexión a internet e intentelo de nuevo.',
      //   type: 'error',
      //   heightAuto: false,
      // });
    }
  }
}
