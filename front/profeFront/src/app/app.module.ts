import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { MaterialModule } from './_shared/material.module';
import { OwlModule } from 'ngx-owl-carousel';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import { AliadosComponent } from './aliados/aliados.component';
import { BlogComponent } from './blog/blog.component';
import { EventsComponent } from './events/events.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LastDonationsComponent } from './last-donations/last-donations.component';
import { CaptainsInfoComponent } from './captains-info/captains-info.component';
import { CampaingInfoComponent } from './campaing-info/campaing-info.component';
import { CounterComponent } from './counter/counter.component'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ContactpageComponent } from './contactpage/contactpage.component';
import { AliadospageComponent } from './aliadospage/aliadospage.component';
import { LegalpageComponent } from './legalpage/legalpage.component';
import { ComopageComponent } from './comopage/comopage.component';
import { DonationpageComponent } from './donationpage/donationpage.component';
import { RegistropageComponent } from './registropage/registropage.component';
import { AliadoComponent } from './aliado/aliado.component';
import { TestimonioComponent } from './testimonio/testimonio.component';
import { ListaBlogComponent } from './lista-blog/lista-blog.component';
import { Pago3dlinkComponent } from './pago3dlink/pago3dlink.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CashComponent } from './pago3dlink/cash/cash.component';
import { PagoMovilComponent } from './pago3dlink/pago-movil/pago-movil.component';
import { TransferenciaComponent } from './pago3dlink/transferencia/transferencia.component';
import { CommonModule } from '@angular/common';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    ContactComponent,
    AliadosComponent,
    BlogComponent,
    EventsComponent,
    GalleryComponent,
    LastDonationsComponent,
    CaptainsInfoComponent,
    CampaingInfoComponent,
    CounterComponent,
    ContactpageComponent,
    AliadospageComponent,
    LegalpageComponent,
    ComopageComponent,
    DonationpageComponent,
    RegistropageComponent,
    AliadoComponent,
    TestimonioComponent,
    ListaBlogComponent,
    Pago3dlinkComponent,
    CashComponent,
    PagoMovilComponent,
    TransferenciaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    OwlModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CommonModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }