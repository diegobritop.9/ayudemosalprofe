import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-blog',
  templateUrl: './lista-blog.component.html',
  styleUrls: ['./lista-blog.component.scss']
})
export class ListaBlogComponent implements OnInit {


  blog = [
    {
      nombre:'Tiffany Cornejo, Comunicadora Social',
      texto:'Siempre he pensado que no hay mayor satisfacción que la que te deja el poder contribuir con el...',
      foto: 'assets/images/tiffany.png',
      url:'/testimonio/1'
    },
    {
      titulo:'Carlos Domingues, Periodista Deportivo',
      extracto:'Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In mollis.',
      imagen: 'assets/images/carlos.png',
      url:'/testimonio/2'
    }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
