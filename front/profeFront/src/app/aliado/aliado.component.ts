import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare const main: any;
@Component({
  selector: 'app-aliado',
  templateUrl: './aliado.component.html',
  styleUrls: ['./aliado.component.scss']
})
export class AliadoComponent implements OnInit {
  
  myId = null;
  titulo = "3D LINK VENEZUELA C.A.";
  texto = "Es un privilegio y un orgullo aportar nuestro conocimiento para el desarrollo de una plataforma que busque hacer aportes significativos en el área deportiva de nuestro país. La unión de fuerzas para concretar este proyecto ha sido posible gracias al compromiso social de 3DLINK, el cual nos obliga a trabajar en proyectos sin fines de lucro como este. El vínculo entre lo deportivo y lo tecnológico constituirá un aporte pequeño pero importante para el deporte que, sin duda, podrá observarse en los resultados positivos de los tiempos por venir.";
  logo = "assets/images/logo3d.png";
  team = "assets/images/3dlink-group.png";
  url = "https://www.3dlinkweb.com";
  contacto = "Diego Torrealba (+58 412-9986725)";

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    main();
    this.myId = this.activatedRoute.snapshot.paramMap.get('id');
    // if(this.myId === "all"){
    //   this.titulo = "ALIADOS";
    // }else{
    //   this.titulo = this.myId;
    // }
    console.log(this.myId);
  }

}
