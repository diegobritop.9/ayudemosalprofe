import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComopageComponent } from './comopage.component';

describe('ComopageComponent', () => {
  let component: ComopageComponent;
  let fixture: ComponentFixture<ComopageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComopageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComopageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
