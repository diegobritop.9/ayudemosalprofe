import { Component, OnInit } from '@angular/core';

declare const main: any;
@Component({
  selector: 'app-comopage',
  templateUrl: './comopage.component.html',
  styleUrls: ['./comopage.component.scss']
})
export class ComopageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    main();
  }

}
