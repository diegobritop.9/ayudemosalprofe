import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {

  blog = [
    {
      titulo:'Tiffany Cornejo, Comunicadora Social',
      extracto:'Siempre he pensado que no hay mayor satisfacción que la que te deja el poder contribuir con el...',
      imagen: 'assets/images/tiffany.png',
      url:'/testimonio/1'
    },
    {
      titulo:'Carlos Domingues, Periodista Deportivo',
      extracto:'Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. In mollis.',
      imagen: 'assets/images/carlos.png',
      url:'/testimonio/2'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
