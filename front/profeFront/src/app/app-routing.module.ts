import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactpageComponent } from './contactpage/contactpage.component';
import { AliadospageComponent } from './aliadospage/aliadospage.component';
import { LegalpageComponent } from './legalpage/legalpage.component';
import { ComopageComponent } from './comopage/comopage.component';
import { DonationpageComponent } from './donationpage/donationpage.component';
import { RegistropageComponent } from './registropage/registropage.component';
import { AliadoComponent } from './aliado/aliado.component';
import { TestimonioComponent } from './testimonio/testimonio.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contact', component: ContactpageComponent},
  {path: 'aliados', component: AliadospageComponent},
  {path: 'aliado/:id', component: AliadoComponent},
  {path: 'legal', component: LegalpageComponent},
  {path: 'comoempezo', component: ComopageComponent},
  {path: 'ayudemos', component: DonationpageComponent},
  {path: 'inscribete', component: RegistropageComponent},
  {path: 'testimonio/:id', component: TestimonioComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
