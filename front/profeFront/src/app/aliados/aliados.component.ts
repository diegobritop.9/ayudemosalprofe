import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-aliados',
  templateUrl: './aliados.component.html',
  styleUrls: ['./aliados.component.scss']
})
export class AliadosComponent implements OnInit {

  aliados = [
    {
      nombre:'3DLink',
      imagen: 'assets/images/3dllink.png',
      texto: 'Soy 3dlink the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliado/3dlink'
    },
    // {
    //   nombre:'Polar',
    //   imagen: 'assets/images/polar.png',
    //   texto: 'Soy polar the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
    //   url: 'http://empresaspolar.com/'
    // },
    {
      nombre:'Hipereventos',
      imagen: 'assets/images/hipereventos.png',
      texto: 'Soy Hipereventos the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliados'
    },
    {
      nombre:'abogados',
      imagen: 'assets/images/law.jpg',
      texto: 'Soy 3dlink the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliados'
    },
    {
      nombre:'Film Maker',
      imagen: 'assets/images/filmaker.png',
      texto: 'Soy filmaker the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life',
      url: '/aliados'
    }
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

}
