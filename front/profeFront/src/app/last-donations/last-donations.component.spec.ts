import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastDonationsComponent } from './last-donations.component';

describe('LastDonationsComponent', () => {
  let component: LastDonationsComponent;
  let fixture: ComponentFixture<LastDonationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastDonationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastDonationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
