@extends('layouts.app')

@section('content')
<div class="main container">
<h1>Profesores</h1>
        <div class="card mt-4">
            <div class="card-header">
                <div class="card-options">
                    <a class="btn btn-primary btn-sm" style="float:right; color:white;" href="{{route('profesors.create')}}"> + Agregar Profe</a>
                    <a class="btn btn-success btn-sm" style="float:right; color:white;" href="{{route('profesors.export')}}"> Descargar Excel</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped text-nowrap table-responsive-lg">
                    <thead>
                        <tr>
                            <th>
                                Profesor ID
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Apellido
                            </th>
                            <th>
                                Carnet
                            </th>
                            <th>
                                Fecha de Inscripcion
                            </th>
                            <th>
                                Estado
                            </th>
                            @if((auth()->user()) && (auth()->user()->isSuperAdmin == 1))
                                    <th>
                                        Acciones
                                    </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($profes as $profe)
                        <tr>
                            <td>
                                @if(auth()->user())
                                    <a href="{{route('profesors.show', $profe->id)}}">
                                        {{$profe->id}}
                                    </a>
                                @else
                                    <a>
                                        {{$profe->id}}
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{$profe->firstName}}
                            </td>
                            <td>
                                {{$profe->lastName}}
                            </td>
                            <td>
                                {{$profe->trainerID}}
                            </td>
                            <td>
                                {{$profe->created_at}}
                            </td>
                            <td>
                                @if($profe->status == 0)
                                <h5><span class="badge badge-danger badge-f">Sin Validar</span></h5> 
                                @elseif($profe->status == 1)
                                <h5><span class="badge badge-primary badge-f">Validado</span></h5> 
                                @elseif($profe->status == 2)
                                <h5><span class="badge badge-info badge-f">Pre Seleccionado</span></h5>
                                @elseif($profe->status == 3)
                                <h5><span class="badge badge-success badge-f">Entregado</span></h5> 
                                @endif
                            </td>
                            @if(auth()->user())
                                @if(auth()->user()->isSuperAdmin == 1)
                                    <td>
                                        <a href="{{route('profesors.edit', $profe->id)}}" class="btn btn-primary">Editar</a>
                                        <form class="d-inline-block" onsubmit="return confirm('Esta seguro de querer eliminar el registro?')" action="{{route('profesors.destroy', $profe->id)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Borrar</button>
                                        </form>
                                    </td>
                                @endif    
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-4 d-flex justify-content-center">
            {{$profes->links()}}
        </div>
</div>
@endsection