@extends('layouts.app')

@section('content')
<div class="main container">
        <h2 class="my-3">Agregar Profe</h2>
        @if($errors->all())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{session()->get('message')}}
            </div>
        @endif
        <form action="{{route('profesors.store')}}" method="post">
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="firstName">Nombre</label>
                    <input type="text" name="firstName" id="firstName" class="form-control" require>
                </div>
                <div class="form-group col-md-6">
                    <label for="lastName">Apellido</label>
                    <input type="text" name="lastName" id="lastName" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="gender">Sexo</label>
                    <select class="form-control" name="gender" id="gender">
                        <option selected hidden>Seleccione uno</option>
                        <option value="Masculino">Masculino</option>
                        <option value="Femenino">Femenino</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="idNumber">Cedula</label>
                    <input type="text" name="idNumber" id="idNumber" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="trainerID">Carnet</label>
                    <input type="text" name="trainerID" id="trainerID" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="phoneHome">Telefono Residencia</label>
                    <input type="text" name="phoneHome" id="phoneHome" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="phoneMobile">Telefono Movil</label>
                    <input type="text" name="phoneMobile" id="phoneMobile" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="address">Direccion</label>
                <input type="text" name="address" id="address" class="form-control">
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="city">Ciudad</label>
                    <input type="text" name="city" id="city" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label for="state">Estado</label>
                    <select class="form-control" name="state" id="state">
                        <option selected hidden>Seleccione uno</option>
                        <option value="Amazonas">Amazonas</option>
                        <option value="Anzoátegui">Anzoátegui</option>
                        <option value="Apure">Apure</option>
                        <option value="Aragua">Aragua</option>
                        <option value="Barinas">Barinas</option>
                        <option value="Bolívar">Bolívar</option>
                        <option value="Carabobo">Carabobo</option>
                        <option value="Cojedes">Cojedes</option>
                        <option value="Delta Amacuro">Delta Amacuro</option>
                        <option value="Distrito Capital">Distrito Capital</option>
                        <option value="Falcón">Falcón</option>
                        <option value="Guárico">Guárico</option>
                        <option value="Lara">Lara</option>
                        <option value="Mérida">Mérida</option>
                        <option value="Miranda">Miranda</option>
                        <option value="Monagas">Monagas</option>
                        <option value="Nueva Esparta">Nueva Esparta</option>
                        <option value="Portuguesa">Portuguesa</option>
                        <option value="Sucre">Sucre</option>
                        <option value="Táchira">Táchira</option>
                        <option value="Trujillo">Trujillo</option>
                        <option value="Vargas">Vargas</option>
                        <option value="Yaracuy">Yaracuy</option>
                        <option value="Zulia">Zulia</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="schoolName">Escuela</label>
                    <input type="text" name="schoolName" id="schoolName" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="categories">Categorias</label>
                    <br>
                    <input type="text" name="categories" id="categories" class="form-control">
                    <!-- <select class="form-control" name="categories[]" id="categories" multiple="multiple">
                        <option value="Compotica">Compotica</option>
                        <option value="Pre-B">Pre-B</option>
                        <option value="Pre-A">Pre-A</option>
                        <option value="Inf-C">Inf-C</option>
                        <option value="Inf-B">Inf-B</option>
                        <option value="Inf-A">Inf-A</option>
                        <option value="Sub-15">Sub-15</option>
                        <option value="Sub-17">Sub-17</option>
                        <option value="Sub-20">Sub-20</option>
                        <option value="3ra. División">3ra. División</option>
                        <option value="2da. División">2da. División</option>
                        <option value="1era. División">1era. División</option>
                    </select> -->
                </div><div class="form-group col-md-2">
                    <label for="futAsociation">Asociacion de futbol</label>
                    <select class="form-control" name="futAsociation" id="futAsociation">
                        <option selected hidden>Seleccione uno</option>
                        <option value="Amazonas">Amazonas</option>
                        <option value="Anzoátegui">Anzoátegui</option>
                        <option value="Apure">Apure</option>
                        <option value="Aragua">Aragua</option>
                        <option value="Barinas">Barinas</option>
                        <option value="Bolívar">Bolívar</option>
                        <option value="Carabobo">Carabobo</option>
                        <option value="Cojedes">Cojedes</option>
                        <option value="Delta Amacuro">Delta Amacuro</option>
                        <option value="Distrito Capital">Distrito Capital</option>
                        <option value="Falcón">Falcón</option>
                        <option value="Guárico">Guárico</option>
                        <option value="Lara">Lara</option>
                        <option value="Mérida">Mérida</option>
                        <option value="Miranda">Miranda</option>
                        <option value="Monagas">Monagas</option>
                        <option value="Nueva Esparta">Nueva Esparta</option>
                        <option value="Portuguesa">Portuguesa</option>
                        <option value="Sucre">Sucre</option>
                        <option value="Táchira">Táchira</option>
                        <option value="Trujillo">Trujillo</option>
                        <option value="Vargas">Vargas</option>
                        <option value="Yaracuy">Yaracuy</option>
                        <option value="Zulia">Zulia</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="yearsActive">Años activo</label>
                    <input type="text" name="yearsActive" id="yearsActive" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="schoolPerson">Persona de contacto de la academia</label>
                    <input type="text" name="schoolPerson" id="schoolPerson" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label for="schoolContact">Telefono de contacto</label>
                    <input type="text" name="schoolContact" id="schoolContact" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="solMessage">Mensaje de solicitud</label>
                <textarea name="solMessage" id="solMessage" cols="30" rows="5" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-info">Guardar</button>
            </div>
        </form>
</div>
@endsection