@extends('layouts.app')

@section('content')
<div class="main container">
        <h2 class="my-3">Editar Datos de Profe</h2>
        @if($errors->all())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
                @endforeach
            </div>
        @endif
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{session()->get('message')}}
            </div>
        @endif
        <form action="{{route('profesors.update', $profesor->id)}}" method="post" >
            @csrf
            @method('put')
            <div class="form-group">
                <label for="firstName">Nombre</label>
                <input type="text" name="firstName" id="firstName" class="form-control" value="{{$profesor->firstName}}">
            </div>
            <div class="form-group">
                <label for="lastName">Apellido</label>
                <input type="text" name="lastName" id="lastName" class="form-control" value="{{$profesor->lastName}}">
            </div>
            <div class="form-group">
                <label for="gender">Sexo</label>
                <select class="form-control" name="gender" id="gender">
                    <option value="Masculino" @if($profesor->gender == 'Masculino') selected @endif>Masculino</option>
                    <option value="Femenino" @if($profesor->gender == 'Femenino') selected @endif>Femenino</option>
                </select>
            </div>
            <div class="form-group">
                <label for="idNumber">Cedula</label>
                <input type="text" name="idNumber" id="idNumber" class="form-control" value="{{$profesor->idNumber}}">
            </div>
            <div class="form-group">
                <label for="trainerID">Carnet</label>
                <input type="text" name="trainerID" id="trainerID" class="form-control" value="{{$profesor->trainerID}}">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" name="email" id="email" class="form-control" value="{{$profesor->email}}">
            </div>
            <div class="form-group">
                <label for="phoneHome">Telefono Residencia</label>
                <input type="text" name="phoneHome" id="phoneHome" class="form-control" value="{{$profesor->phoneHome}}">
            </div>
            <div class="form-group">
                <label for="phoneMobile">Telefono Movil</label>
                <input type="text" name="phoneMobile" id="phoneMobile" class="form-control" value="{{$profesor->phoneMobile}}">
            </div>
            <div class="form-group">
                <label for="city">Ciudad</label>
                <input type="text" name="city" id="city" class="form-control" value="{{$profesor->city}}">
            </div>
            <div class="form-group">
                <label for="state">Estado</label>
                <select class="form-control" name="state" id="state">
                    <option selected hidden>Seleccione uno</option>
                    <option value="Amazonas" @if($profesor->state == 'Amazonas') selected @endif>Amazonas</option>
                    <option value="Anzoátegui" @if($profesor->state == 'Anzoátegui') selected @endif>Anzoátegui</option>
                    <option value="Apure" @if($profesor->state == 'Apure') selected @endif>Apure</option>
                    <option value="Aragua" @if($profesor->state == 'Aragua') selected @endif>Aragua</option>
                    <option value="Barinas" @if($profesor->state == 'Barinas') selected @endif>Barinas</option>
                    <option value="Bolívar" @if($profesor->state == 'Bolívar') selected @endif>Bolívar</option>
                    <option value="Carabobo" @if($profesor->state == 'Carabobo') selected @endif>Carabobo</option>
                    <option value="Cojedes" @if($profesor->state == 'Cojedes') selected @endif>Cojedes</option>
                    <option value="Delta Amacuro" @if($profesor->state == 'Delta Amacuro') selected @endif>Delta Amacuro</option>
                    <option value="Distrito Capital" @if($profesor->state == 'Distrito Capital') selected @endif>Distrito Capital</option>
                    <option value="Falcón" @if($profesor->state == 'Falcón') selected @endif>Falcón</option>
                    <option value="Guárico" @if($profesor->state == 'Guárico') selected @endif>Guárico</option>
                    <option value="Lara" @if($profesor->state == 'Lara') selected @endif>Lara</option>
                    <option value="Mérida" @if($profesor->state == 'Mérida') selected @endif>Mérida</option>
                    <option value="Miranda" @if($profesor->state == 'Miranda') selected @endif>Miranda</option>
                    <option value="Monagas" @if($profesor->state == 'Monagas') selected @endif>Monagas</option>
                    <option value="Nueva Esparta" @if($profesor->state == 'Nueva Esparta') selected @endif>Nueva Esparta</option>
                    <option value="Portuguesa" @if($profesor->state == 'Portuguesa') selected @endif>Portuguesa</option>
                    <option value="Sucre" @if($profesor->state == 'Sucre') selected @endif>Sucre</option>
                    <option value="Táchira" @if($profesor->state == 'Táchira') selected @endif>Táchira</option>
                    <option value="Trujillo" @if($profesor->state == 'Trujillo') selected @endif>Trujillo</option>
                    <option value="Vargas" @if($profesor->state == 'Vargas') selected @endif>Vargas</option>
                    <option value="Yaracuy" @if($profesor->state == 'Yaracuy') selected @endif>Yaracuy</option>
                    <option value="Zulia" @if($profesor->state == 'Zulia') selected @endif>Zulia</option>
                </select>
            </div>
            <div class="form-group">
                <label for="schoolName">Escuela</label>
                <input type="text" name="schoolName" id="schoolName" class="form-control" value="{{$profesor->schoolName}}">
            </div>
            <div class="form-group">
                <label for="categories">Categorias</label>
                <br>
                <select class="form-control" name="categories[]" id="categories" multiple="multiple">
                    <option value="Compotica" @if(in_array('Compotica',$profesor->categories)) selected @endif>Compotica</option>
                    <option value="Pre-B" @if(in_array('Pre-B',$profesor->categories))) selected @endif>Pre-B</option>
                    <option value="Pre-A" @if(in_array('Pre-A',$profesor->categories))) selected @endif>Pre-A</option>
                    <option value="Inf-C" @if(in_array('Inf-C',$profesor->categories))) selected @endif>Inf-C</option>
                    <option value="Inf-B" @if(in_array('Inf-B',$profesor->categories))) selected @endif>Inf-B</option>
                    <option value="Inf-A" @if(in_array('Inf-A',$profesor->categories))) selected @endif>Inf-A</option>
                    <option value="Sub-15" @if(in_array('Sub-15',$profesor->categories))) selected @endif>Sub-15</option>
                    <option value="Sub-17" @if(in_array('Sub-17',$profesor->categories))) selected @endif>Sub-17</option>
                    <option value="Sub-20" @if(in_array('Sub-20',$profesor->categories))) selected @endif>Sub-20</option>
                    <option value="3ra. División" @if(in_array('3ra. División',$profesor->categories))) selected @endif>3ra. División</option>
                    <option value="2da. División" @if(in_array('2da. División',$profesor->categories))) selected @endif>2da. División</option>
                    <option value="1era. División" @if(in_array('1era. División',$profesor->categories))) selected @endif>1era. División</option>
                </select>
            </div>
            <div class="form-group">
                <label for="yearsActive">Años activo</label>
                <input type="text" name="yearsActive" id="yearsActive" class="form-control" value="{{$profesor->yearsActive}}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-outline-info">Guardar</button>
            </div>
        </form>
</div>
@endsection