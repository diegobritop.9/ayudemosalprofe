@extends('layouts.app')
@section('content')
<div class="main container">

    <div class="form-group">
        <label for="status" class="">Estado en sistema</label>
        <div class="col-md-6">
            @if($profesor->status == 0)
            <h5><span class="badge badge-danger badge-f">Sin Validar</span></h5> 
            @elseif($profesor->status == 1)
            <h5><span class="badge badge-primary badge-f">Validado</span></h5> 
            @elseif($profesor->status == 2)
            <h5><span class="badge badge-info badge-f">Pre Seleccionado</span></h5>
            @elseif($profesor->status == 3)
            <h5><span class="badge badge-success badge-f">Entregado</span></h5> 
            @endif
        </div>
    </div>
<div class="row">
    <div class="form-group">
        <label for="firstName" class="">Nombre</label>
        <div class="col">
            <input type="text" name="firstName" id="firstName" class="form-control" value="{{$profesor->firstName}}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="lastName" class="">Apellido</label>
        <div class="col">
            <input type="text" name="lastName" id="lastName" class="form-control" value="{{$profesor->lastName}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="gender" class="">Sexo</label>
        <div class="col">
            <input type="text" name="gender" id="gender" class="form-control" value="{{$profesor->gender}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="idNumber" class="">Cedula</label>
        <div class="col">
            <input type="text" name="idNumber" id="idNumber" class="form-control" value="{{$profesor->idNumber}}" disabled>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label for="carnet" class="">Carnet</label>
        <div class="col">
            <input type="text" name="carnet" id="carnet" class="form-control" value="{{$profesor->trainerID}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="email" class="">Email</label>
        <div class="col">
            <input type="email" name="email" id="email" class="form-control" value="{{$profesor->email}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="phoneHome" class="">Telefono Residencia</label>
        <div class="col">
            <input type="text" name="phoneHome" id="phoneHome" class="form-control" value="{{$profesor->phoneHome}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="phoneMobile" class="">Telefono Movil</label>
        <div class="col">
            <input type="text" name="phoneMobile" id="phoneMobile" class="form-control" value="{{$profesor->phoneMobile}}" disabled>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label for="city" class="">Ciudad</label>
        <div class="col">
            <input type="text" name="city" id="city" class="form-control" value="{{$profesor->city}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="state" class="">Estado</label>
        <div class="col">
            <input type="text" name="state" id="state" class="form-control" value="{{$profesor->state}}" disabled>
        </div>
    </div>
    
    <div class="form-group">
        <label for="schoolName" class="">Escuela</label>
        <div class="col">
            <input type="text" name="schoolName" id="schoolName" class="form-control" value="{{$profesor->schoolName}}" disabled>
        </div>
    </div>

    <div class="form-group">
        <label for="categories" class="">Categorias</label>
        <div class="col">
            <input type="text" name="categories" id="categories" class="form-control" value="{{$profesor->categories}}" disabled>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group">
        <label for="yearsActive" class="">Años activo</label>
        <div class="col">
            <input type="text" name="yearsActive" id="yearsActive" class="form-control" value="{{$profesor->yearsActive}}" disabled>
        </div>
    </div>
</div>

<div class="class">
    <div class="card-body">
        <form action="{{route('comments.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="comment">Agregar Comentario</label>
                <textarea name="comment" id="comment" cols="30" rows="4" class="form-control"></textarea>
                <input type="hidden" name="profesor_id" id="profesor_id" value='{{$profesor->id}}'>
            </div>
            @if(auth()->user())
                <!-- @if(auth()->user()->isSuperAdmin == 1) -->
                    <div class="form-group">
                            <label for="status">Cambiar Status</label>
                            <select class="form-control" name="status" id="status">
                                <option selected disabled hidden>Seleccione una opcion</option>
                                <option value="0">Sin Validar</option>
                                <option value="1">Validado</option>
                                <option value="2">Pre Seleccionado</option>
                                <option value="3">Entregado</option>
                            </select>
                    </div>
                <!-- @endif -->
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-outline-info">Guardar</button>
            </div>
        </form>
    </div>
</div>


    
@if($comments !=null && $comments->count()>0)
    <div class="form-group">
        @foreach($comments as $comment)
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>{{$comment->content}}</p>
                <footer class="blockquote-footer">{{$comment->user->name}} | {{$comment->user->email}} {{$comment->created_at->formatLocalized('%d %B %Y %I:%M')}}{{$comment->created_at->format('a')}}</footer>
            </blockquote>
        </div>
        {{-- <label for="created-by">Comment By {{$comment->user->name}}| {{$comment->user->email}}</label>
        <p>{{$comment->content}}</p> --}}
        @endforeach
    </div>
@else
<div class="form-group">
    <p>Sin comentarios</p>
</div>
@endif
    
    
    
</div>
@endsection