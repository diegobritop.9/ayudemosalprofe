@extends('layouts.app')

@section('content')
<div class="main container">
<meta name="csrf-token" content="{{ csrf_token() }}">
@if($errors->all())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{session()->get('message')}}
    </div>
@endif
        <h1>Publicaciones</h1>
        {{-- @foreach ($photos as $photo) --}}
        <div class="card mt-4">
            <div class="card-header">
                <div class="card-options">
                    <a class="btn btn-primary btn-sm" style="float:right; color:white;" href="{{route('photos.create')}}"> + Agregar Foto</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped text-nowrap table-responsive-lg">
                    <thead>
                        <tr>
                            <th>
                                ID
                            </th>
                            <th>
                                Miniatura
                            </th>
                            <th>
                                Mostrar en Pagina Principal
                            </th>
                            <th>
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($photos as $photo)
                            <tr>
                                <td>
                                    <a href="{{route('photos.show', $photo->id)}}">
                                        {{$photo->id}}    
                                    </a>
                                </td>
                                <td>
                                    <img class="avatar" src="{{url('uploads/photos/'.$photo->picture)}}" alt="service_image">      
                                </td>
                                <td>
                                    @if($post->featured > 0)   
                                        <div class="custom-control custom-switch">
                                            <input checked type="checkbox" class="custom-control-input" id="switch{{$post->id}}" onchange="featured({{$post->id}},this)">
                                            <label class="custom-control-label" for="switch{{$post->id}}"></label>
                                        </div>
                                    @else
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="switch{{$post->id}}" onchange="featured({{$post->id}},this)">
                                            <label class="custom-control-label" for="switch{{$post->id}}"></label>
                                        </div>
                                    @endif    
                                </td>
                                <td>
                                    <a href="{{route('photos.edit', $post->id)}}" class="btn btn-info">Edit</a>
                                    <form class="d-inline-block" onsubmit="return confirm('Are you sure you want to delete this post?')" action="{{route('photos.destroy', $post->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-4 d-flex justify-content-center">
            {{$photos->links()}}
        </div>
</div>

@endsection