@extends('layouts.app')
@section('content')
<div class="main container">
    <div class="card mb-3">
        <div class="card-header">
            <h2>{{$post->title}}</h2>
        </div>
        <div class="card-body">
            <p>
                {!!$post->content!!}
            </p>
            
            <div>
                <span>Tipo de Publicacion:</span> {{$post->category->name}}    
            </div>
            <div>
                <span>Autor:</span> {{$post->author}}    
            </div>
            <div>
                <span>Editor:</span> {{$post->user->name}}    
            </div>
            <div>
                <span>Fecha:</span> {{$post->created_at->formatLocalized('%d %B %Y %I:%M')}}{{$post->created_at->format('a')}}    
            </div>
            <div>
                <label for="picture">Imagen:</label>
                <br>
                <img src="{{url('uploads/'.$post->picture1)}}" style="max-width:100%;" alt="posts_image">
            </div>
            @if($post->picture2)
            <div>
                <label for="picture">Imagen:</label>
                <br>
                <img src="{{url('uploads/'.$post->picture2)}}" style="max-width:100%;" alt="posts_image">
            </div>
            @endif

        </div>
    </div>
</div>
@stop