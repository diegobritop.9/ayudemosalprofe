@extends('layouts.app')
@section('content')
<div class="main container">
    <h2 class="my-3">Update post</h2>
    @if($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif
    <form action="{{route('posts.update', $post->id)}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="title">Titulo</label>
            <input name="title" id="title" class="form-control" value="{{$post->title}}">
        </div>
        <div class="form-group">
            <label for="content">Contenido</label>
            <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{$post->content}}</textarea>
        </div>
        <div class="form-group">
            <label for="author">Autor</label>
            <input name="author" id="author" class="form-control" value="{{$post->author}}">
        </div>
        <div class="form-group">
            <label for="category">Categoria</label>
            <select name="category" id="category">
                @foreach($categories as $category)
                    <option name="category" value="{{$category->id}}" @if($category->id == $post->category->id) selected @endif>{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Imagen Actual</label>
            <img class="avatar" src="/../aliriocv/storage/img/posts_pictures/{{$post->picture}}" alt="post_image">
        </div>
        <div class="form-group">
            <label>Cambiar Imagen</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="picture" name="picture">
                <label class="custom-file-label" for="picture">Seleccione un archivo</label>
            </div>
        </div>
        <div class="form-group">
            <label>Imagen Actual 2</label>
            <img class="avatar" src="/../aliriocv/storage/img/posts_pictures/{{$post->picture2}}" alt="post_image">
        </div>
        <div class="form-group">
            <label>Cambiar Imagen 2</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="picture2" name="picture2">
                <label class="custom-file-label" for="picture2">Seleccione un archivo</label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Actualizar Publicacion</button>
        </div>
    </form>
</div>
@endsection