@extends('layouts.app')

@section('content')
<div class="main container">
<meta name="csrf-token" content="{{ csrf_token() }}">
@if($errors->all())
    <div class="alert alert-danger">
        @foreach ($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach
    </div>
@endif

@if(session()->has('message'))
    <div class="alert alert-success">
        {{session()->get('message')}}
    </div>
@endif
        <h1>Publicaciones</h1>
        {{-- @foreach ($posts as $post) --}}
        <div class="card mt-4">
            <div class="card-header">
                <div class="card-options">
                    <a class="btn btn-primary btn-sm" style="float:right; color:white;" href="{{route('posts.create')}}"> + Agregar Publicacion</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped text-nowrap table-responsive-lg">
                    <thead>
                        <tr>
                            <th>
                                Titulo
                            </th>
                            <th>
                                Autor
                            </th>
                            <th>
                                Editor
                            </th>
                            <th>
                                Categoria
                            </th>
                            <th>
                                Mostrar en Pagina Principal
                            </th>
                            <th>
                                Acciones
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach ($posts as $post)
                            <tr>
                                <td>
                                    <a href="{{route('posts.show', $post->id)}}">
                                        {{$post->title}}    
                                    </a>
                                </td>
                                <td>
                                    @if($post->author)
                                        {{$post->author}}
                                    @else
                                        N/A
                                    @endif        
                                </td>
                                <td>
                                    @if($post->user_id)
                                    {{$post->user->name}}
                                    @else
                                        N/A
                                    @endif    
                                </td>
                                <td>
                                    @if($post->category_id)
                                    {{$post->category->name}}
                                    @else
                                    N/A
                                    @endif
                                </td>
                                <td>
                                    @if($post->featured > 0)   
                                        <div class="custom-control custom-switch">
                                            <input checked type="checkbox" class="custom-control-input" id="switch{{$post->id}}" onchange="featured({{$post->id}},this)">
                                            <label class="custom-control-label" for="switch{{$post->id}}"></label>
                                        </div>
                                    @else
                                        <div class="custom-control custom-switch">
                                            <input type="checkbox" class="custom-control-input" id="switch{{$post->id}}" onchange="featured({{$post->id}},this)">
                                            <label class="custom-control-label" for="switch{{$post->id}}"></label>
                                        </div>
                                    @endif    
                                </td>
                                <td>
                                    <a href="{{route('posts.edit', $post->id)}}" class="btn btn-info">Edit</a>
                                    <form class="d-inline-block" onsubmit="return confirm('Are you sure you want to delete this post?')" action="{{route('posts.destroy', $post->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-4 d-flex justify-content-center">
            {{$posts->links()}}
        </div>
</div>

@endsection