@extends('layouts.app')
@section('content')
<div class="main container">
    <h2 class="my-3">Crear Publicacion</h2>
    @if($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </div>
    @endif
    <form action="{{route('posts.store')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Titulo</label>
            <input type="text" name="title" id="title" class="form-control">
        </div>
        <div class="form-group">
            <label for="content">Contenido</label>
            <textarea name="content" id="content" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label for="author">Autor</label>
            <input type="text" name="author" id="author" class="form-control">
        </div>
        <div class="form-group">
            <label for="category">Tipo de Publicacion</label>
            <select name="category" id="category">
                @foreach($categories as $category)
                    <option name="category" value="{{$category->id}}">{{$category->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Imagen 1</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="picture" name="picture">
                <label class="custom-file-label" for="picture">Choose File</label>
            </div>
        </div>
        <div class="form-group">
            <label>Imagen 2</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="picture2" name="picture2">
                <label class="custom-file-label" for="picture2">Choose File</label>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Crear Publicacion</button>
        </div>
    </form>
@endsection
</div>
