<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'AProfe Dashboard') }}</title>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/avatar.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'AProfe Dashboard') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Profes <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{route('profesors.webIndex')}}">Todos los Profes</a></li>
                                @if((auth()->user()) && (auth()->user()->isSuperAdmin == 1))
                                    <li><a class="nav-link" href="{{route('profesors.create')}}">Agregar Profe</a></li>
                                @endif
                            </ul>
                        </div>
                        <!-- <div class="nav-item">
                            <a class="nav-link" href="{{route('doners.webIndex')}}" data-toggle="dropdown">Donaciones</a>
                        </div> -->
                        <li class="nav-item">
                            <a href="{{route('doners.webIndex')}}" class="nav-link">Donaciones</a>
                        </li>
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Publicaciones <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{route('posts.webIndex')}}">Todas las Publicaciones</a></li>
                                @if((auth()->user()))
                                    <li><a class="nav-link" href="{{route('posts.create')}}">Agregar Publicacion</a></li>
                                @endif
                            </ul>
                        </div>
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Tipos de Publicaciones <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{route('categories.index')}}">Todas los Categorias</a></li>
                                @if((auth()->user()))
                                    <li><a class="nav-link" href="{{route('categories.create')}}">Agregar Categoria</a></li>
                                @endif
                            </ul>
                        </div>
                        <!-- <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Pagos <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="{{route('profesors.webIndex')}}">Todos los Profes</a></li>
                                @if((auth()->user()))
                                    <li><a class="nav-link" href="{{route('profesors.create')}}">Agregar Profe</a></li>
                                @endif
                            </ul>
                        </div> -->
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $("#categories").select2();
        $("#gender").select2();
        $("#state").select2();
    </script> -->
    <script src="{{asset('/js/file_name_upload.js')}}"></script>
    <script src="{{asset('/js/postFeatured.js')}}"></script>
</body>
</html>
