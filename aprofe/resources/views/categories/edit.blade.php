@extends('layouts.app')
@section('content')
<div class="main container">
    <h2 class="my-3">Modificar Tipo de Publicacion</h2>
    @if($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </div>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{session()->get('message')}}
        </div>
    @endif
    <form action="{{route('categories.update', $category->id)}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="name">Nombre</label>
            <input name="name" id="name" class="form-control" value="{{$category->name}}">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Modificar</button>
        </div>
    </form>
</div>
@endsection