@extends('layouts.app')
@section('content')
<div class="main container">
    <h2 class="my-3">Agregar Tipo de Publicacion</h2>
    @if($errors->all())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </div>
    @endif
    <form action="{{route('categories.store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Nombre</label>
            <input type="text" name="name" id="name" class="form-control">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-outline-info">Guardar</button>
        </div>
    </form>
</div>
@endsection