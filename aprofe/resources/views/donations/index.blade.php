@extends('layouts.app')

@section('content')
<div class="main container">
<h1>Donaciones</h1>
        <div class="card mt-4">
            <div class="card-header">
                <div class="card-options">
                    <a class="btn btn-success btn-sm" style="float:right; color:white;" href="{{route('doners.export')}}"> Descargar Excel</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped text-nowrap table-responsive-lg">
                    <thead>
                        <tr>
                            <th>
                                id
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Apellido
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                #Confirmacion
                            </th>
                            <th>
                                Estado
                            </th>
                            <th>
                                Fecha de Donacion
                            </th>
                            @if((auth()->user()) && (auth()->user()->isSuperAdmin == 1))
                                    <th>
                                        Acciones
                                    </th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($doners as $doner)
                        <tr>
                            <td>
                                @if(auth()->user())
                                    <a href="{{route('doners.show', $doner->id)}}">
                                        {{$doner->id}}
                                    </a>
                                @else
                                    <a>
                                        {{$doner->id}}
                                    </a>
                                @endif
                            </td>
                            <td>
                                @if($doner->name)
                                {{$doner->name}}
                                @else
                                    <a>Anonimo</a>
                                @endif
                            </td>
                            <td>@if($doner->lastName)
                                {{$doner->lastName}}
                                @else
                                    <a>Anonimo</a>
                                @endif
                            </td>
                            <td>
                                {{$doner->email}}
                            </td>
                            <td>
                                {{$doner->transaction_number}}
                            </td>
                            <td>
                                @if($doner->confirmation_status == 0)
                                <h5><span class="badge badge-danger badge-f">Sin Confirmar</span></h5> 
                                @elseif($doner->confirmation_status == 1)
                                <h5><span class="badge badge-success badge-f">Confirmado</span></h5> 
                                @endif
                            </td>
                            <td>
                                @if($doner->date)
                                {{$doner->date->formatLocalized('%d %B %Y %I:%M')}}
                                @else
                                    <a>Sin Registrar</a>
                                @endif
                            </td>
                            @if(auth()->user())
                                @if(auth()->user()->isSuperAdmin == 1)
                                    <td>
                                        <form class="d-inline-block" onsubmit="return confirm('Esta seguro de querer eliminar el registro?')" action="{{route('doners.destroy', $doner->id)}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Borrar</button>
                                        </form>
                                    </td>
                                @endif    
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="mt-4 d-flex justify-content-center">
            {{$doners->links()}}
        </div>
</div>
@endsection