@extends('layouts.app')
@section('content')
<div class="main container">

    <div class="form-group">
        <label for="confirmation_status" class="">Estado de Confirmacion</label>
        <div class="col-md-6">
            @if($doner->confirmation_status == 0)
            <h5><span class="badge badge-danger badge-f">Sin Validar</span></h5>
            @elseif($doner->confirmation_status == 1)
            <h5><span class="badge badge-success badge-f">confirmado</span></h5> 
            @endif
        </div>
    </div>
<div class="row">
    <div class="form-group col-md-6">
        <label for="name" class="">Nombre</label>
        <div class="col">
            <input type="text" name="name" id="name" class="form-control" value="{{$doner->name}}" disabled>
        </div>
    </div>

    <div class="form-group col-md-6">
        <label for="lastName" class="">Apellido</label>
        <div class="col">
            <input type="text" name="lastName" id="lastName" class="form-control" value="{{$doner->lastName}}" disabled>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        <label for="phone" class="">Telefono</label>
        <div class="col">
            <input type="text" name="phone" id="phone" class="form-control" value="{{$doner->phone}}" disabled>
        </div>
    </div>
    
    <div class="form-group col-md-6">
        <label for="email" class="">Email</label>
        <div class="col">
            <input type="text" name="email" id="email" class="form-control" value="{{$doner->email}}" disabled>
        </div>
    </div>
</div>


<div class="row">
    <div class="form-group col-md-4">
        <label for="transaction_type" class="">Tipo de Tansaccion</label>
        <div class="col">
            <input type="text" name="transaction_type" id="transaction_type" class="form-control" value="{{$doner->transaction_type}}" disabled>
        </div>
    </div>
<!-- Si es Zelle -->
@if($doner->transaction_type == "zelle")
    <div class="form-group col-md-4">
        <label for="holder" class="">Titular</label>
        <div class="col">
            <input type="text" name="holder" id="holder" class="form-control" value="{{$doner->holder}}" disabled>
        </div>
    </div>

<!-- Si no (Pago Movil, Transferencia, Deposito)-->
@else
    <div class="form-group col-md-4">
        <label for="issuing_bank" class="">Banco Emisor</label>
        <div class="col">
            <input type="text" name="issuing_bank" id="issuing_bank" class="form-control" value="{{$doner->issuing_bank}}" disabled>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label for="receiving_bank" class="">Banco Receptor</label>
        <div class="col">
            <input type="text" name="receiving_bank" id="receiving_bank" class="form-control" value="{{$doner->receiving_bank}}" disabled>
        </div>
    </div>

@endif
</div>
<div class="row">
    <div class="form-group col-md-4">
        <label for="currency" class="">Moneda</label>
        <div class="col">
            <input type="text" name="currency" id="currency" class="form-control" value="{{$doner->currency}}" disabled>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label for="ammount" class="">Monto</label>
        <div class="col">
            <input type="text" name="ammount" id="ammount" class="form-control" value="{{$doner->ammount}}" disabled>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label for="transaction_number" class="">Numero de Confirmacion</label>
        <div class="col">
            <input type="text" name="transaction_number" id="transaction_number" class="form-control" value="{{$doner->transaction_number}}" disabled>
        </div>
    </div>
    <div class="form-group col-md-4">
        <label for="date" class="">Fecha</label>
        <div class="col">
            @if($doner->date)
            <input type="text" name="date" id="date" class="form-control" value="{{$doner->date->formatLocalized('%d %B %Y %I:%M')}}" disabled>
            @else
            <input type="text" name="date" id="date" class="form-control" value="Sin Fecha Agregada" disabled>
            @endif   
        </div>
    </div>
</div>



<div class="row">
    <div class="form-group">
        <label for="transaction_number" class="">Observaciones</label>
        <div class="col">
            <p>
                {!!$doner->observations!!}
            </p>
        </div>
    </div>
</div>

<div class="class">
    <div class="card-body">
        <form action="{{route('paymentComments.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="comment">Agregar Comentario</label>
                <textarea name="comment" id="comment" cols="30" rows="4" class="form-control"></textarea>
                <input type="hidden" name="doner_id" id="doner_id" value='{{$doner->id}}'>
            </div>
            @if(auth()->user())
                <!-- @if(auth()->user()->isSuperAdmin == 1) -->
                    <div class="form-group">
                            <label for="status">Cambiar Status</label>
                            <select class="form-control" name="status" id="status">
                                <option selected disabled hidden>Seleccione una opcion</option>
                                <option value="0">Sin Confirmar</option>
                                <option value="1">Confirmado</option>
                            </select>
                    </div>
                <!-- @endif -->
            @endif
            <div class="form-group">
                <button type="submit" class="btn btn-outline-info">Guardar</button>
            </div>
        </form>
    </div>
</div>


    
@if($paymentComments != null && $paymentComments->count()>0)
    <div class="form-group">
        @foreach($paymentComments as $comment)
        <div class="card-body">
            <blockquote class="blockquote mb-0">
                <p>{{$comment->content}}</p>
                <footer class="blockquote-footer">{{$comment->user->name}} | {{$comment->user->email}} {{$comment->created_at->formatLocalized('%d %B %Y %I:%M')}}{{$comment->created_at->format('a')}}</footer>
            </blockquote>
        </div>
        {{-- <label for="created-by">Comment By {{$comment->user->name}}| {{$comment->user->email}}</label>
        <p>{{$comment->content}}</p> --}}
        @endforeach
    </div>
@else
<div class="form-group">
    <p>Sin comentarios</p>
</div>
@endif
    
    
    
</div>
@endsection