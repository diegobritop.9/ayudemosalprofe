<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'profesor_id', 'user_id', 'content'
    ];

    public function profesor(){
        return $this->belongsTo('App\Profesor');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    //
}
