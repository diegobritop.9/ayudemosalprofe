<?php

namespace App\Exports;

use App\Profesor;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class ProfesorsExport implements FromQuery
{
    use Exportable;
    /**
    * @return \Illuminate\Support\FromQuery
    */
    public function query()
    {
        return Profesor::query();
    }
}
