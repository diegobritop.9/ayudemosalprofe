<?php

namespace App\Exports;

use App\Doner;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;

class DonersExport implements FromQuery
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Query
    */
    public function query()
    {
        return Doner::query();
    }
}
