<?php

namespace App\Http\Controllers;

use App\Doner;
use App\Exports\DonersExport;
use Excel;
use Illuminate\Http\Request;

class DonerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function export() 
    {
        return Excel::download(new DonersExport, 'donaciones.xlsx');
    }

    public function index()
    {
        $doners = Doner::all();
        return $doners;
        //
    }
    
    public function webIndex()
    {
        $doners = Doner::paginate(10);
        return view('donations.index',compact('doners'));
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->date = date('Y-m-d H:i:s');
        $doner = Doner::create($request->all());
        return $doner;
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doner  $doner
     * @return \Illuminate\Http\Response
     */
    public function show(Doner $doner)
    {
        $paymentComments = $doner->comments->sortByDesc('created_at');
        //dd($profesor);
        return view('donations.show', compact('doner','paymentComments'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doner  $doner
     * @return \Illuminate\Http\Response
     */
    public function edit(Doner $doner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doner  $doner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doner $doner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doner  $doner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doner $doner)
    {
        //
    }
}
