<?php

namespace App\Http\Controllers;

use App\Profesor;
use App\Exports\ProfesorsExport;
use Excel;
use Illuminate\Http\Request;

class ProfesorController extends Controller
{
    /* public function __construct(){
        $this->middleware('auth');
    } */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function export() 
    {
        return Excel::download(new ProfesorsExport, 'profes.xlsx');
    }

    public function index()
    {
        $profes = Profesor::all();
        return $profes;
        //
    }

    public function webIndex()
    {
        $profes = Profesor::orderBy('lastName','desc')->paginate(10);
        return view('profesors.index',compact('profes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profesors.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $profe = Profesor::create($request->all());
        return $profe;
        //
    }

    public function webStore(Request $request)
    {
        $request['status'] = 0;
        //dd($request);
        $profe = Profesor::create($request->all());
        session()->flash('message','Profesor agregado exitosamente');
        return redirect(route('profesors.webIndex'));
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */
    public function show(Profesor $profesor)
    {
        $comments = $profesor->comments->sortByDesc('created_at');
        //dd($profesor);
        return view('profesors.show', compact('profesor','comments'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */
    public function edit(Profesor $profesor)
    {
        return view('profesors.edit', compact('profesor'));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profesor $profesor)
    {
        //dd($profesor);
        unset($request['_token'], $request['_method']);
        $profesor = Profesor::whereId($profesor->id)->update($request->all());
        return redirect(route('profesors.webIndex'));

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profesor  $profesor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profesor $profesor)
    {
        $profesor->delete();
        return redirect(route('profesors.webIndex'));
        //
    }
}
