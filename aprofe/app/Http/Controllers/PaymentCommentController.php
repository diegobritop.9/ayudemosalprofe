<?php

namespace App\Http\Controllers;

use App\PaymentComment;
use App\Doner;
use Illuminate\Http\Request;

class PaymentCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $this->validate($request,[
            'comment' => 'required|min:6'
        ]);
        PaymentComment::create([
            'user_id' => auth()->user()->id,
            'doner_id' => $request->doner_id,
            'content' => $request->comment
        ]);
        $doner = Doner::find($request->doner_id);
        if(isset($request->status)){
            if($request->status != $doner->confirmation_status){
                $doner->confirmation_status = $request->status;        
            }
        }
        $doner->save();
        //session()->flash('message','Your comment has been added');
        return redirect()->back()->with('message','Your comment has been added');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentComment  $paymentComment
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentComment $paymentComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentComment  $paymentComment
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentComment $paymentComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentComment  $paymentComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentComment $paymentComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentComment  $paymentComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentComment $paymentComment)
    {
        //
    }
}
