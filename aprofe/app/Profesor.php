<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $fillable = ["firstName", "lastName", "city", "state", "schoolName", "yearsActive", "trainerID", "categories", "idNumber", "email", "phoneHome", "phoneMobile", "gender", "status"];
    //
    protected $casts = [
        'categories' => 'array',
    ];



    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
