<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doner extends Model
{
    protected $fillable = ["name", "lastName", "email", "phone", "ammount", "combo", "currency", "date", "transaction_number", "issuing_bank", "receiving_bank", "holder", "observations", "confirmation_status", "transaction_type"];

    protected $dates = ['date'];
    //
    
    public function comments(){
        return $this->hasMany('App\PaymentComment');
    }
}
