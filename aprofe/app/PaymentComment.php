<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentComment extends Model
{
    protected $fillable = [
        'doner_id', 'user_id', 'content'
    ];

    public function doner(){
        return $this->belongsTo('App\Doner');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
    //
}
