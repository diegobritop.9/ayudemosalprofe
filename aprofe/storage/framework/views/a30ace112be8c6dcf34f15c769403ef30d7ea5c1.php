<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'AProfe Dashboard')); ?></title>

    <!-- Scripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/avatar.css')); ?>" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                    <?php echo e(config('app.name', 'AProfe Dashboard')); ?>

                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Profes <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="<?php echo e(route('profesors.webIndex')); ?>">Todos los Profes</a></li>
                                <?php if((auth()->user()) && (auth()->user()->isSuperAdmin == 1)): ?>
                                    <li><a class="nav-link" href="<?php echo e(route('profesors.create')); ?>">Agregar Profe</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- <div class="nav-item">
                            <a class="nav-link" href="<?php echo e(route('doners.webIndex')); ?>" data-toggle="dropdown">Donaciones</a>
                        </div> -->
                        <li class="nav-item">
                            <a href="<?php echo e(route('doners.webIndex')); ?>" class="nav-link">Donaciones</a>
                        </li>
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Publicaciones <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="<?php echo e(route('posts.webIndex')); ?>">Todas las Publicaciones</a></li>
                                <?php if((auth()->user())): ?>
                                    <li><a class="nav-link" href="<?php echo e(route('posts.create')); ?>">Agregar Publicacion</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Tipos de Publicaciones <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="<?php echo e(route('categories.index')); ?>">Todas los Categorias</a></li>
                                <?php if((auth()->user())): ?>
                                    <li><a class="nav-link" href="<?php echo e(route('categories.create')); ?>">Agregar Categoria</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- <div class="dropdown nav-item">
                            <a class="nav-link dropdown-toggle" href="" data-toggle="dropdown">Pagos <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a class="nav-link" href="<?php echo e(route('profesors.webIndex')); ?>">Todos los Profes</a></li>
                                <?php if((auth()->user())): ?>
                                    <li><a class="nav-link" href="<?php echo e(route('profesors.create')); ?>">Agregar Profe</a></li>
                                <?php endif; ?>
                            </ul>
                        </div> -->
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                            </li>
                            <?php if(Route::has('register')): ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                                </li>
                            <?php endif; ?>
                        <?php else: ?>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <?php echo e(__('Logout')); ?>

                                    </a>

                                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                        <?php echo csrf_field(); ?>
                                    </form>
                                </div>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php echo $__env->yieldContent('content'); ?>
        </main>
    </div>
    
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $("#categories").select2();
        $("#gender").select2();
        $("#state").select2();
    </script> -->
    <script src="<?php echo e(asset('/js/file_name_upload.js')); ?>"></script>
    <script src="<?php echo e(asset('/js/postFeatured.js')); ?>"></script>
</body>
</html>
<?php /**PATH C:\laragon\www\aprofe\resources\views/layouts/app.blade.php ENDPATH**/ ?>