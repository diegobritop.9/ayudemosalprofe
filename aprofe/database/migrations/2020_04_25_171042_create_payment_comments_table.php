<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index('fk_user_paycomm_id');
            $table->bigInteger('doner_id')->unsigned()->index('fk_doner_id');
            $table->text('content');
            $table->timestamps();

            $table->foreign('doner_id','fk_doner_id')->references('id')->on('doners')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('user_id','fk_user_paycomm_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_comments');
    }
}
