<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfesorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesors', function (Blueprint $table) {
            $table->id();
            $table->string('firstName');
            $table->string('lastName');
            $table->string('city');
            $table->string('state');
            $table->string('schoolName');
            $table->integer('yearsActive');
            $table->json('categories');
            $table->string('trainerID');
            $table->string('idNumber');
            $table->string('email');
            $table->string('phoneHome');
            $table->string('phoneMobile');
            $table->string('gender');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesors');
    }
}
