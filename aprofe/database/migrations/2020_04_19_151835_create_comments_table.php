<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->index('fk_user_comm_id');
            $table->bigInteger('profesor_id')->unsigned()->index('fk_profesor_id');
            $table->text('content');
            $table->timestamps();

            $table->foreign('profesor_id','fk_profesor_id')->references('id')->on('profesors')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('user_id','fk_user_comm_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
