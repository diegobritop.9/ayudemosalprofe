<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateProfesorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profesors', function (Blueprint $table) {
            $table->string('schoolName')->nullable()->change();
            $table->integer('yearsActive')->nullable()->change();
            $table->string('categories')->nullable()->change();
            $table->string('futAsociation')->nullable();
            $table->string('schoolPerson')->nullable();
            $table->string('schoolContact')->nullable();
            $table->text('solMessage')->nullable();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
