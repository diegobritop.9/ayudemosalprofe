<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDonersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('doners', function (Blueprint $table) {
            $table->string('currency');
            $table->integer('transaction_number');
            $table->string('issuing_bank')->nullable();
            $table->string('receiving_bank')->nullable();
            $table->string('holder')->nullable();
            $table->text('observations')->nullable();
            $table->integer('confirmation_status');
            $table->string('transaction_type');
            $table->date('date')->nullable();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
