<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
Route::get('/', 'HomeController@index')->name('home');

Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profesors','ProfesorController@webIndex')->name('profesors.webIndex');
Route::get('/profesors/create','ProfesorController@create')->name('profesors.create');
Route::post('/profesors/store','ProfesorController@webStore')->name('profesors.store');
Route::put('/profesors/update/{profesor}','ProfesorController@update')->name('profesors.update');
Route::get('/profesors/show/{profesor}','ProfesorController@show')->name('profesors.show');
Route::get('/profesors/edit/{profesor}','ProfesorController@edit')->name('profesors.edit');
Route::delete('/profesors/destroy/{profesor}','ProfesorController@destroy')->name('profesors.destroy');
Route::get('/profesors/export', 'ProfesorController@export')->name('profesors.export');

Route::get('/doners','DonerController@webIndex')->name('doners.webIndex');
Route::get('/doners/show/{doner}','DonerController@show')->name('doners.show');
Route::get('/doners/export', 'DonerController@export')->name('doners.export');

Route::get('/posts','PostController@webIndex')->name('posts.webIndex');
Route::get('/posts/create','PostController@create')->name('posts.create');
Route::post('/posts/store','PostController@webStore')->name('posts.store');
Route::put('/posts/update/{post}','PostController@update')->name('posts.update');
Route::get('/posts/show/{post}','PostController@show')->name('posts.show');
Route::get('/posts/edit/{post}','PostController@edit')->name('posts.edit');
Route::delete('/posts/destroy/{post}','PostController@destroy')->name('posts.destroy');
Route::post('/posts/{post}/setfeatured','PostController@setFeatured');
Route::post('/posts/{post}/unsetfeatured','PostController@unsetFeatured');

Route::resource('categories','CategoryController');

Route::resource('comments','CommentController');

Route::resource('paymentComments', 'PaymentCommentController');

